#! /usr/bin/env python
import time

from db_session import DBSession
from user import User
from random import randint
from config import Config
from copy import deepcopy


def now_timestamp():
    return int(time.time())


class Periodic:

    conf = Config.get_config()

    q1_update_interval = float(conf["intervals"]["q1_update_interval"]) * 60
    q1_last_update = now_timestamp()

    q2_update_interval = float(conf["intervals"]["q2_update_interval"]) * 60
    q2_last_update = now_timestamp()

    drop_update_interval = float(conf["intervals"]["drop_update_interval"]) * 60
    drop_last_update = now_timestamp()

    other_queue_interval_min = float(conf["intervals"]["other_queue_interval_min"]) * 60
    other_queue_interval_max = float(conf["intervals"]["other_queue_interval_max"]) * 60
    other_queue_intervals = {}
    other_queue_last_updates = {}

    @classmethod
    def call(cls):
        now = now_timestamp()
        session = None
        updated = False
        try:

            if now - cls.drop_last_update > cls.drop_update_interval:
                session = DBSession.get_session()
                cls._drop_inactive(session)
                session.commit()
                cls.drop_last_update = now
                updated = True


            if now - cls.q1_last_update > cls.q1_update_interval:
                session = DBSession.get_session()
                cls._make_turn(session, qlvl=1)
                session.commit()
                cls.q1_last_update = now
                updated = True


            if now - cls.q2_last_update > cls.q2_update_interval:
                session = DBSession.get_session()
                cls._make_turn_only_active(session, qlvl=2)
                session.commit()
                cls.q2_last_update = now
                updated = True


            session = DBSession.get_session()
            qlvls = cls._select_qlvls(session)
            if qlvls:
                for qlvl in qlvls:
                    if qlvl and qlvl > 2 and qlvl not in cls.other_queue_intervals:
                        cls.other_queue_intervals[qlvl] = qlvl * randint(cls.other_queue_interval_min, cls.other_queue_interval_max)
                        cls.other_queue_last_updates[qlvl] = now
                        print ("Interval was set for Q%d: %d" % (qlvl, cls.other_queue_intervals[qlvl]))
                need_commit = False
                for qlvl, q_interval in cls.other_queue_intervals.items():
                    q_last_update = cls.other_queue_last_updates[qlvl]
                    if now - q_last_update > q_interval:
                        cls._make_turn_only_active(session, qlvl)
                        cls.other_queue_last_updates[qlvl] = now
                        need_commit = True
                if need_commit:
                    session.commit()
                    updated = True

            return updated

        finally:
            if session:
                session.close()

    @classmethod
    def _make_turn(cls, session, qlvl):
        session.query(User).filter(User.qlvl == qlvl, User.qplace != None).\
            update({"qplace": User.qplace-1}, synchronize_session='fetch')
        cls._level_up(session, qlvl)


    @classmethod
    def _make_turn_only_active(cls, session, qlvl):
        session.query(User).filter(User.qlvl == qlvl, User.qplace != None, User.active == 1).\
            update({"qplace": User.qplace-1}, synchronize_session='fetch')
        cls._level_up(session, qlvl)


    @classmethod
    def _level_up(cls, session, qlvl):
        next_qlvl = qlvl + 1
        next_q_last_place = User.get_last_place_in_queue(next_qlvl)
        if not next_q_last_place:
            next_q_last_place = 0
        session.query(User).filter(User.qlvl == qlvl, User.qplace == 0).\
            update({"qlvl": next_qlvl, "qplace": next_q_last_place+1}, synchronize_session='fetch')

    @classmethod
    def _drop_inactive(cls, session):
        session.query(User).filter(User.qlvl > 2, User.active == 0).\
            update({"qplace": None}, synchronize_session='fetch')

    @classmethod
    def _select_qlvls(cls, session):
        rows = session.query(User.qlvl).group_by(User.qlvl).all()
        qlvls = []
        for row in rows:
            qlvls.append(row.qlvl)
        return qlvls

    @classmethod
    def get_update_intervals(cls):
        update_intervals = deepcopy(cls.other_queue_intervals)
        update_intervals[1] = cls.q1_update_interval
        update_intervals[2] = cls.q2_update_interval
        return update_intervals
