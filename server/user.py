from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy import Column, Integer, String, func
from db_session import DBSession


class User(Base):
    __tablename__ = 'user'
    uid = Column(String, primary_key=True)
    name = Column(String)
    lobby_reg_token = Column(String)
    lobby_reg_token_upd_date = Column(String)
    qlvl = Column(Integer)
    qplace = Column(Integer)
    active = Column(Integer)
    last_active_date = Column(String)

    @classmethod
    def get_queue_members(cls, qlvl):
        session = DBSession.get_session()
        try:
            rows = session.query(User).filter(User.qlvl == qlvl, User.qplace!=None).order_by(User.qplace, User.last_active_date.desc()).all()
            return rows
        finally:
            session.close()

    @classmethod
    def get_top(cls, topsize=5):
        session = DBSession.get_session()
        try:
            rows = session.query(User).filter(User.qlvl!=None).order_by(User.qlvl.desc(), User.qplace==None, User.qplace, User.last_active_date.desc()).limit(topsize).all()
            return rows
        finally:
            session.close()

    @classmethod
    def get_max_qlvl(cls):
        session = DBSession.get_session()
        try:
            row = session.query(func.max(User.qlvl).label("max_qlvl")).one()
            return row.max_qlvl
        finally:
            session.close()

    @classmethod
    def get_last_place_in_queue(cls, qlvl):
        session = DBSession.get_session()
        try:
            row = session.query(func.max(User.qplace).label("last_place")).filter(User.qlvl == qlvl).one()
            return row.last_place
        finally:
            session.close()

    def jsonable(self):
        return {"uid": self.uid, "name": self.name}

    def update_active(self, active):
        if active:
            active = 1
        else:
            active = 0

        session = DBSession.get_session()
        try:
            user = session.query(User).filter_by(uid=self.uid).one()
            user.active = active
            user.last_active_date = func.now()
            session.add(user)
            session.commit()
        finally:
            session.close()

    @classmethod
    def set_all_inactive(cls):
        session = DBSession.get_session()
        try:
            session.query(User).filter(User.active == 1).update({"active": 0}, synchronize_session='fetch')
            session.commit()
        finally:
            session.close()
