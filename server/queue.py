class Queue:

    def __init__(self, qlvl):
        self.players = []
        self.qlvl = qlvl

    def add_player(self, player):
        self.players.append(player)

    def set_players(self, players):
        self.players = players

    def jsonable(self):
        return {"qlvl": self.qlvl, "players": self.players}
