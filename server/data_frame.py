import json


class DataFrame:
    def __init__(self, action, data=None):
        self.action = action
        self.data = data

    def jsonable(self):
        return {"action": self.action, "data": self.data}

    def encode(self):
        return json.dumps(self, default=_JSONEncodeHandler)


def _JSONEncodeHandler(Obj):
    if hasattr(Obj, 'jsonable'):
        return Obj.jsonable()
    else:
        raise Exception(TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(Obj), repr(Obj)))
