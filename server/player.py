import logging
import tornado.escape
from user import User
from db_session import DBSession


class Player:
    def __init__(self, connection, uid):
        self.connection = connection
        self.uid = uid
        self.disconnected = False
        self.name = "?"
        self.active = True

        session = DBSession.get_session()
        try:
            model = session.query(User).filter(User.uid == self.uid).one()
            self.name = tornado.escape.xhtml_escape(model.name)

            need_commit = False
            if not model.qlvl:
                model.qlvl = 1
                need_commit = True
            if not model.qplace:
                last_place = User.get_last_place_in_queue(model.qlvl)
                if not last_place:
                    last_place = 1
                else:
                    last_place = last_place + 1
                model.qplace = last_place
                need_commit = True

            if need_commit:
                session.add(model)
                session.commit()
        except:
            logging.error("Error getting user's model", exc_info=True)
        finally:
            session.close()

    def jsonable(self):
        return {"uid": self.uid, "name": self.name}

    def send(self, data_frame):
        if self.disconnected:
            return None

        try:
            self.connection.send(data_frame.encode())
        except:
            logging.error("Error sending message", exc_info=True)

    def get_model(self):
        session = DBSession.get_session()
        try:
            return session.query(User).filter(User.uid == self.uid).one()
        except:
            logging.error("Error getting user's model", exc_info=True)
        finally:
            session.close()

    def flush_reg_token(self):
        session = DBSession.get_session()
        try:
            model = self.get_model()
            model.lobby_reg_token = ""
            session.add(model)
            session.commit()
        finally:
            session.close()

    def set_active(self):
        self.active = True
        self.update_active()

    def set_inactive(self):
        self.active = False
        self.update_active()

    def update_active(self):
        self.get_model().update_active(self.active)
