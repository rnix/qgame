import configparser
import os.path


class Config:
    instance = None
    @classmethod
    def get_config(cls):
        if not Config.instance:
            base_config_dir = os.path.dirname(__file__) + '/../config'
            app_env_file = base_config_dir + '/APP_ENV'
            with open(app_env_file, "r") as app_env_file_handler:
                Config.app_env = app_env_file_handler.read()

            conf = configparser.ConfigParser()
            conf.readfp(open(base_config_dir + '/config_base.ini'))
            conf = dict(conf)

            env_conf = configparser.ConfigParser()
            env_conf.readfp(open(base_config_dir + '/config_' + Config.app_env + '.ini'))
            env_conf = dict(env_conf)

            for section, values in conf.items():
                conf[section].update(env_conf[section])

            Config.instance = dict(conf)
        return Config.instance
