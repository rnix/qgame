#!/usr/bin/env python
#

import logging
import uuid
import re

from tornado import escape, web, ioloop
from sockjs.tornado import SockJSRouter, SockJSConnection
from config import Config
from player import Player
from user import User
from data_frame import DataFrame
from queue import Queue
from periodic import Periodic


class SocketHandler(SockJSConnection):
    connections = {}
    players = {}
    q1 = Queue(1)
    q2 = Queue(2)
    q2 = Queue(3)
    q4 = Queue(4)

    @classmethod
    def _convert_camelcase(cls, csstring):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', csstring)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def on_open(self, info):
        self.id = str(uuid.uuid4())
        SocketHandler.connections[self.id] = self

    def on_close(self):
        player = SocketHandler.get_player_by_connection(connection=self)
        if player:
            player.disconnected = True
            player.set_inactive()

        SocketHandler.connections.pop(self.id, None)

    def on_message(self, message):
        logging.info("got message %r", message)
        parsed = escape.json_decode(message)

        if "action" in parsed:
            method_player = "_" + "on_" + SocketHandler._convert_camelcase(parsed["action"]) + "_player_action"
            method_connection = "_" + "on_" + SocketHandler._convert_camelcase(parsed["action"]) + "_connection_action"
            player = SocketHandler.get_player_by_connection(self)

            if method_connection in dir(SocketHandler):
                getattr(SocketHandler, method_connection)(data=parsed, connection=self)
            elif player and method_player in dir(SocketHandler):
                getattr(SocketHandler, method_player)(data=parsed, player=player)
            else:
                logging.info("Trying to call unknown action %r", parsed["action"])

    @classmethod
    def _on_get_info_connection_action(cls, data, connection):
        cls.broadcast_queues_info()

    @classmethod
    def _on_register_connection_action(cls, data, connection):
        if SocketHandler.get_player_by_connection(connection):
            return
        if "uid" in data and "userLobbyToken" in data:
            player = Player(connection, uid=data["uid"])
            model = player.get_model()
            if player and model and model.lobby_reg_token == data["userLobbyToken"]:
                SocketHandler.players[player.uid] = player
                df = DataFrame("registered", player)
                player.send(df)
                player.flush_reg_token()
                player.set_active()
                cls.broadcast_queues_info()
            else:
                try:
                    df = DataFrame("error", "register_fail")
                    connection.send(df.encode())
                except:
                    logging.error("Register error", exc_info=True)

    @classmethod
    def _on_get_general_info_connection_action(cls, data, connection):
        df = DataFrame("generalInfo", cls.get_general_info())
        connection.send(df.encode())

    @classmethod
    def get_general_info(cls):
        top = {}
        i = 0
        for user in User.get_top():
            i=i+1
            top[i] = {"name": user.name, "qlvl": user.qlvl}

        return {
        "update_intervals": Periodic.get_update_intervals(), 
        "top": top
        }

    @classmethod
    def broadcast_general_info(cls):
        df = DataFrame("generalInfo", cls.get_general_info())
        for iconnection in SocketHandler.connections.values():
            iconnection.send(df.encode())

    @classmethod
    def get_player_by_connection(cls, connection):
        for iplayer in SocketHandler.players.values():
            if connection.session.session_id == iplayer.connection.session.session_id:
                return iplayer

    @classmethod
    def broadcast_queues_info(cls):
        cls.q1 = cls.get_queue(1)
        cls.q2 = cls.get_queue(2)
        cls.q3 = cls.get_queue(3)
        maxqlvl = User.get_max_qlvl()
        if maxqlvl and maxqlvl > 3:
            cls.q4 = cls.get_queue(maxqlvl)
        else:
            cls.q4 = cls.get_queue(4)
        queues_info = {"q1": cls.q1, "q2": cls.q2, "q3": cls.q3, "q4": cls.q4}
        for iconnection in SocketHandler.connections.values():
            player = SocketHandler.get_player_by_connection(iconnection)
            if player:
                pqlvl = player.get_model().qlvl
                if pqlvl and pqlvl > 3 and pqlvl != cls.q4.qlvl:
                    queues_info["q3"] = cls.get_queue(pqlvl)
            df = DataFrame("info", queues_info)
            iconnection.send(df.encode())

    @classmethod
    def get_queue(cls, qlvl):
        q = Queue(qlvl)
        q.set_players(User.get_queue_members(qlvl))
        return q

    @classmethod
    def _on_active_player_action(cls, data, player):
        player.set_active()

    @classmethod
    def _on_inactive_player_action(cls, data, player):
        player.set_inactive()

    @classmethod
    def periodic(cls):
        updated = Periodic.call()
        if updated:
            cls.broadcast_queues_info()
            cls.broadcast_general_info()


def main():
    logging.basicConfig(level=logging.DEBUG)
    SocketRouter = SockJSRouter(SocketHandler, '/socket')
    app = web.Application(SocketRouter.urls)
    port = Config.get_config()["general"]["port"]
    app.listen(port)
    User.set_all_inactive()
    ioloop_inst = ioloop.IOLoop.instance()
    periodic = ioloop.PeriodicCallback(callback=SocketHandler.periodic, callback_time=3000, io_loop=ioloop_inst)
    periodic.start()
    ioloop_inst.start()

if __name__ == "__main__":
    main()
