from sqlalchemy import *
from sqlalchemy.orm import *
from config import Config


class DBSession:
    Session = None

    @classmethod
    def get_session(cls):
        if not DBSession.Session:
            conf = Config.get_config()
            dsn = "mysql+mysqlconnector://" + conf["db"]["username"] \
                + ":" + conf["db"]["password"] + "@" + conf["db"]["host"] + "/" + conf["db"]["dbname"]
            engine = create_engine(dsn, pool_recycle=3600)
            DBSession.Session = sessionmaker(bind=engine)
        return DBSession.Session()
