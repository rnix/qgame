<?php

$appEnvFile = __DIR__ . '/../config/APP_ENV';
if (file_exists($appEnvFile)){
    $env = trim(file_get_contents($appEnvFile));
    if ($env){
        defined('APP_ENV') || define('APP_ENV', $env);
    }
}

$sharedConfig = parse_ini_file(__DIR__ . '/../config/config_base.ini', true);
if (!defined('APP_ENV')){
    echo "PUT FILE $appEnvFile WHICH CONTAINS VALUE OF APP_ENV. SEE " . __FILE__;
    return;
} else {
    $sharedEnvConfigFile = __DIR__ . '/../config/config_' . APP_ENV . '.ini';
    if (file_exists($sharedEnvConfigFile)){
        $sharedEnvConfig = parse_ini_file($sharedEnvConfigFile, true);
        
        foreach ($sharedEnvConfig as $section=>$values){
            foreach ($values as $name=>$value){
                $sharedConfig[$section][$name] = $value;
            }
        }
        
        foreach ($sharedConfig as $section => $values) {
            foreach ($values as $name => $value) {
                $value = str_replace('__DIR__', __DIR__ . '/../config', $value);
                if ($value === '1') {
                    $value = true;
                } else if ($value === '0') {
                    $value = false;
                }
                $sharedConfig[$section][$name] = $value;
            }
        }
        
        
    } else {
        echo "MAKE INI FILE $sharedEnvConfigFile. SEE " . __FILE__;
        return;
    }
}

$configDir = dirname(__FILE__) . '/../protected/config';

$yii = $sharedConfig['env']['yii_path'];
defined('YII_DEBUG') or define('YII_DEBUG', $sharedConfig['env']['yii_debug']);
if (isset($sharedConfig['env']['yii_enable_error_handler'])){
    defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', $sharedConfig['env']['yii_enable_error_handler']);
}
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);

$configMain = require_once( $configDir . '/main.php' );
$envYiiConfigFile = $configDir . '/config_' . APP_ENV . '.php';
if (file_exists($envYiiConfigFile)) {
    $configEnv = require_once( $envYiiConfigFile );
    $config = CMap::mergeArray($configMain, $configEnv);
} else {
    $config = $configMain;
}


Yii::createWebApplication($config)->run();