<?php

class WebUser extends CWebUser {
    private $_model = null;
    protected $service;
 
    public function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->getId());
        }
        return $this->_model;
    }
    
    public function login($identity, $duration = 0) {
        $this->service = $identity->service;
        $user = User::model()->findByAttributes(array('lll_user_id' => $this->service->id));
        if ($user === null) {
            $user = new User;
            $user->lll_user_id = $this->service->id;
            $user->name = $this->service->name;
        }
        $user->lll_access_token = $this->service->getStoredAccessToken();
        $user->lll_token_expired_in = $this->service->getStoredAccessTokenExpires();
        $user->save();
        $identity->setState('uid', $user->uid);
        $identity->setState('isAnon', false);

        return parent::login($identity, $duration);
    }
    
    public function loginAsAnonymous(){
        $user = new User;
        $user->name = 'Anon' . mt_rand();
        $user->save();
        $this->setState('uid', $user->uid);
        $this->setState('isAnon', true);
        $this->setName($user->name);
    }


    public function getId(){
        return $this->getState('uid');
    }
    
    public function isAnon(){
        return $this->getState('isAnon');
    }
    
    public function getIsGuest() {
        return !$this->isAnon() && parent::getIsGuest();
    }
}