
$(function(){
    
    if (typeof webSocketAddress == 'undefined'){
        alert("Ошибка сервера. Зайдите позже.");
        return;
    }
    
    //typeof siteUid == 'undefined' || typeof siteUserLobbyToken == 'undefined'
    
    var conMgr = new ConnectionManager(webSocketAddress);
    var gc = $(".game")[0];
    var myPlayer;
    var myLastQLvl = 0;
    var inactiveMessageWasShown = false;
    
    conMgr.setDebug(siteDebugMode);
    conMgr.bind("onOpen", start);
    conMgr.bind("onClose", function(){
        $(".connection-wait").hide();
        $(".connection-lost").show();
    });
    conMgr.connect();
    
    function start(){
        $(".connection-wait").hide();
        $(gc).show();
        conMgr.send({"action": "getInfo"});
        
        if (typeof siteUid !== 'undefined' && typeof siteUserLobbyToken !== 'undefined'){
            register();
        }
        
        conMgr.bind("onGeneralInfo", updateGeneralInfo);
        conMgr.send({"action": "getGeneralInfo"});
    }
    
    conMgr.bind("onError", function(error){
        $(".btn-to-queue", gc).button('reset');
        $(".on-error-container").html(error).show();
        window.setTimeout(function(){
            $(".on-error-container").hide();
        }, 3000);
    });
    
    conMgr.bind("onInfo", function(info){
        updateQs(info);
    });
    
    
    function updateGeneralInfo(info){
        var update_intervals = info.update_intervals;
        for (var qlvl in update_intervals){
            var minutes = Math.round(update_intervals[qlvl] / 60);
            $('.queue[data-qlvl="' + qlvl + '"] .qinterval', gc).html(minutes + " мин");
        }
        
        var ctx = $(".top-conatiner", gc)[0];
        for (var place in info.top){
            var player = info.top[place];
            $('.place' + place, ctx).html(player.name + '<sup>' + player.qlvl + '</sup>');
        }
    }
    
    
    function updateQs(info){
        updateQ(info['q1'], 1);
        updateQ(info['q2'], 2);
        updateQ(info['q3'], 3);
        updateQ(info['q4'], 4);
    }
    
    
    function updateQ(qInfo, qInd){
        var $tpl = $('.tpl-player-row', gc);
        $('.queue'+qInd+' .qlvl', gc).html(qInfo.qlvl);
        $('.queue'+qInd, gc).attr('data-qlvl', qInfo.qlvl);
        var $container = $('.queue'+qInd+' .qtable', gc).empty();
        $container.removeClass('my-queue');
        for (var playerInd in qInfo.players){
            var playerInfo = qInfo.players[playerInd];
            var $row = $tpl.clone().removeClass('tpl-player-row');
            $row.find('td').html(playerInfo.name);
            if (myPlayer && myPlayer.uid == playerInfo.uid){
                $row.addClass('info');
                $container.addClass('my-queue');
                myLastQLvl = qInfo.qlvl;
            }
            $container.append($row);
        }
    }
    
    function register(){
        var data = {
            "action": "register", 
            "uid": siteUid,
            "userLobbyToken": siteUserLobbyToken
        };
        conMgr.send(data);
    }
    
    $(".btn-to-queue", gc).click(function(){
        if (typeof siteUid == 'undefined' || typeof siteUserLobbyToken == 'undefined'){
            $(".need-auth-modal").modal();
        } else {
            $(this).button('loading');
            register();
        }
    });
    
    conMgr.bind("onRegistered", function(playerInfo){
        $(".btn-to-queue", gc).button('reset').hide();
        myPlayer = playerInfo;
    });
    
    
    $(".modal-clost-btn", gc).click(function(){
        $(this).closest('.modal').modal('hide');
    });
    
        
    var onWindowActiveChangeLocalFunc = function(state){
        var data = {
            "action": state==='visible' ? "active" : "inactive" 
        };
        conMgr.send(data);
        
        if (!$('.my-queue', gc).length && myLastQLvl > 2 && !inactiveMessageWasShown){
            $(".was-inactive-modal").modal();
            inactiveMessageWasShown = true;
        }
    }
    window.onWindowActiveChange = onWindowActiveChangeLocalFunc;
    /* trying to prevent disabling window.onWindowActiveChange function */
    window.setInterval(function(){
        if (typeof window.onWindowActiveChange !== 'function' || window.onWindowActiveChange.toString() !== onWindowActiveChangeLocalFunc.toString()){
            conMgr.close();
        }
    }, 500);
    
    $(".reconnect-btn").click(function(){
        location.reload(true);
    });
    
    
    //+ Jonas Raoni Soares Silva
    //@ http://jsfromhell.com/array/shuffle [v1.0]
    function shuffle(o){ //v1.0
        for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };
    
    (function(){
        var tweets = [
            "Кто крайний?", 
            "За мной не занимать.", 
            "Я только спросить.",
            "Вас тут не стояло.",
            "Кто последний?",
            "Ой, а я вас не заметила!",
            "Сначала пройдите в левое окно.",
            "Ну что вы толкаетесь? Аккуратней можно?",
            "Я занимал, просто отошел.",
            "Я стоял вон за тем в синем.",
            "Я здесь с самого утра стою.",
            "Я крайний, но за мной еще женщина, она отошла.",
        ];
        var text = shuffle(tweets).shift();
        $(".twitter-share-button").attr('data-text', text);
    })()
});
    