var ConnectionManager = function(webSocketAddress){
    this.webSocketAddress = webSocketAddress;
    this.debug = false;
    this.bindings = {};
    this.sock = null;
}

ConnectionManager.prototype.connect = function(){
    var sock = new SockJS(this.webSocketAddress);
    var that = this;
    
    sock.onopen = function() {
        that._callBindings('onOpen');
    };
    sock.onmessage = function(evt) {
        that._onMessage(evt);
    };
    sock.onclose = function() {
        that._callBindings('onClose');
    };
    
    this.sock = sock;
}

ConnectionManager.prototype.close = function(){
    this.sock.close();
}

ConnectionManager.prototype._callBindings = function(method, data){
    var found = false;
    var dataParam = data || null;
    if (typeof this.bindings[method] === 'object'){
        for (var i = 0; i < this.bindings[method].length; i++){
            this.bindings[method][i](dataParam);
            found = true;
        }
    }
                
    if (!found && this.debug && method!='onOpen' && method!='onClose') {
        console.warn('method not found: ' + method);
    }
}

ConnectionManager.prototype._onMessage = function(evt){
    var input = JSON && JSON.parse(evt.data) || $.parseJSON(evt.data);
    if (this.debug) console.log('input', input);
    var method = 'on' + input.action.charAt(0).toUpperCase() + input.action.slice(1);
    var data = input.data;
    
    this._callBindings(method, data);
}

ConnectionManager.prototype.bind = function(action, callback){
    if (typeof this.bindings[action] == "undefined"){
        this.bindings[action] = [];
    }
    this.bindings[action].push(callback);
}

ConnectionManager.prototype.send = function(data){
    if (this.debug) {
        console.log('output', data);
    }
    this.sock.send(JSON.stringify(data));
}

ConnectionManager.prototype.setDebug = function(state){
    this.debug = state;
}