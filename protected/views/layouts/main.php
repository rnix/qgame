<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link  rel="shortcut icon" href="/favicon.ico">

        <?php
        $myPackage = array(
            'basePath' => 'application.views.assets',
            'css' => array('css/main.css'),
            'js' => array('js/main.js',),
            'depends' => array('jquery')
        );

        Yii::app()->assetManager->publish(Yii::getPathOfAlias($myPackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
        Yii::app()->clientScript->addPackage('myPack', $myPackage)->registerPackage('myPack');
        ?>

        <?php Yii::app()->bootstrap->register(); ?>
    </head>

    <body>

        <?php
        $this->widget('bootstrap.widgets.TbNavbar', array(
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'items' => array(
                        array('label' => 'О проекте', 'url' => array('/about'), 'active'=>$this->action->id=='about'),
                        array('label' => 'Играть', 'url' => array('/index'), 'active'=>$this->action->id=='index'),
                        array('label' => 'Войти', 'url' => array('/login'), 'visible' => Yii::app()->user->isGuest, 'active'=>$this->action->id=='login'),
                        array('label' => 'Выйти (' . Yii::app()->user->name . ')', 'url' => array('logout'), 'visible' => !Yii::app()->user->isGuest)
                    ),
                ),
            ),
        ));
        ?>

        <div class="container" id="page">

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>
            
            <hr>
            <footer>
                <p><a href="https://twitter.com/share" class="twitter-share-button" data-text="Эта очередь мне подходит" data-lang="ru" data-size="large" data-count="none" data-hashtags="queuegame">Твитнуть</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                </p>
                <p>
                    <a href="http://livelevel.net">LiveLevel.net</a> - там есть ссылки на другие игрульки.
                </p>
            </footer>
        </div><!-- page -->
        
        <?php if (!(defined('YII_DEBUG') && YII_DEBUG)) { ?>
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter22268377 = new Ya.Metrika({id:22268377,
                                clickmap:true});
                        } catch(e) { }
                    });
                    
                    var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
                    
                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
            </script>
            <noscript><div><img src="//mc.yandex.ru/watch/22268377" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
        <?php } ?>
    </body>
</html>
