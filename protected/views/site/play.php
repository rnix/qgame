<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<script>
    var checkersAssetsUrl = "<?= $gameAssetsUrl; ?>";
</script>

<div>

    <div class="alert alert-info connection-wait">
        <h3>Происходит подключение</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: 100%;"></div>
        </div>
    </div>

    <div class="alert alert-warn connection-lost" style="display: none;">
        Соединение с сервером закрыто. <a href="#" class="reconnect-btn">Подключиться снова</a>
    </div>
    <div class="alert alert-danger connection-error" style="display: none;">
        Соединение с сервером не удалось.
    </div>

    <div class="alert alert-danger on-error-container" style="display: none;">

    </div>


    <!--    game-->
    <div class="game" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="span3 queue queue1">
                    <h3>Окно №<span class="qlvl">1</span> - <span class="qinterval" title="Интервал обновления"></span></h3>
                    <table class="qtable table table-striped table-condensed"></table>
                    <p class="descr">
                        Очередь не требует постоянного вашего присутствия. Вы просто занимаете очередь, а двигаться она будет сама вместе с вами.
                    </p>
                    <p><button type="button" class="btn-to-queue btn btn-default" data-loading-text="Loading..." autocomplete="off">Занять »</button></p>
                </div>
                <div class="span3 queue queue2">
                    <h3>Окно №<span class="qlvl">2</span> <span class="qinterval"></span></h3>
                    <table class="qtable table table-striped table-condensed"></table>
                    <p class="descr">
                        Очередь временами требует вашего присутствия. А именно, в момент движения. Если вы отлучитесь, а очереди надо двигаться,
                        то ничего страшного, вы просто пропустите вперед себя несколько человек.
                    </p>
                    <p><a href="#" class="btn-to-queue btn btn-default">Занять »</a></p>
                </div>
                <div class="span3 queue queue3">
                    <h3>Окно №<span class="qlvl">3</span> <span class="qinterval"></span></h3>
                    <table class="qtable table table-striped table-condensed"></table>
                    <p class="descr">
                        Эта и последующие очереди требуют постоянного вашего присутствия. Если вы отойдёте, то занимать брошенную очередь придеться с начала.
                    </p>
                    <p><a href="#" class="btn-to-queue btn btn-default">Занять »</a></p>
                </div>
                <div class="span3 queue queue4">
                    <h3>Окно №<span class="qlvl">4</span> <span class="qinterval"></span></h3>
                    <table class="qtable table table-striped table-condensed"></table>
                    <p class="descr">
                        Здесь мы показываем максимальную достигнутую кем-либо очередь.
                    </p>
                    <p><a href="#" class="btn-to-queue btn btn-default">Занять »</a></p>
                </div>
            </div>
            
            <div class="row top-conatiner">
                
                <div class="span2">
                    <h4>Топ 5</h4>
                </div>
                <div class="span2">
                    <div class="well place1"></div>
                </div>
                <div class="span2">
                    <div class="well place2"></div>
                </div>
                <div class="span2">
                    <div class="well place3"></div>
                </div>
                <div class="span2">
                    <div class="well place4"></div>
                </div>
                <div class="span2">
                    <div class="well place5"></div>
                </div>
            </div>
        </div>
        
        <div style="display: none;">
            <table>
                <tr class="tpl-player-row">
                    <td>_name_</td>
                </tr>
            </table>

        </div>
        
        <div class="need-auth-modal modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Мы вас не знаем</h3>
            </div>
            <div class="modal-body">
                <p>Для участия в очереди нужно войти на сайт через Твиттер или Вконтакте. Нажмите синюю кнопку для продолжения. Не пугайтесь, если на пути будет сайт livelevel.net - это часть всего этого.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn modal-clost-btn">Закрыть</a>
                <a href="/login" class="btn btn-primary">Войти</a>
            </div>
        </div>
        
        <div class="was-inactive-modal modal hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Вы всё пропустили</h3>
            </div>
            <div class="modal-body">
                <p>В находитесь в той очереди, где нельзя отвлекаться ни на секунду. Теперь, видимо, нужно снова занять очередь.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn modal-clost-btn">Закрыть сообщение</a>
                <a href="/" class="btn btn-primary">Занять очередь</a>
            </div>
        </div>
        
        
        
    </div>
    <!--    end game-->


</div>