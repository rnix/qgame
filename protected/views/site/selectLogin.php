<?php ?>
<ul class="thumbnails">
    <li class="span4">
        <div class="thumbnail">
            <div class="caption">
                <h3>Вход через Livelevel.net</h3>
                <p>Позволяет учитывать ваши достижения. Аутентификация происходит через Twitter или VK.</p>
                <p align="center"><a href="<?=$this->createUrl('site/login')?>" class="btn btn-primary btn-block">Войти</a></p>
            </div>
        </div>
    </li>
    <li class="span4">
        <div class="thumbnail">
            <div class="caption">
                <h3>Играть как гость</h3>
                <p>Ваши успехи не будут храниться долго. Вам будет назначено случайное имя.</p>
                <p align="center"><a href="<?=$this->createUrl('site/anonLogin')?>" class="btn btn-block">Попробовать</a></p>
            </div>
        </div>
    </li>
</ul>