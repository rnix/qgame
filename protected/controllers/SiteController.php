<?php

class SiteController extends Controller {

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function actionAbout(){
        $this->render('about');
    }
    
    public function actionLogin() {
        
        if (!empty($_GET['error'])){
            throw new CException($_GET['error']);
        }
                
        $authIdentity = Yii::app()->eauth->getIdentity('livelevel');
        $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
        $authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login');

        if ($authIdentity->authenticate()) {
            $identity = new MyEAuthUserIdentity($authIdentity);
            
            if ($identity->authenticate()) {
                $duration = 3600 * 24 * 30; // 30 days
                Yii::app()->user->login($identity, $duration);
                
                // special redirect with closing popup window
                $authIdentity->redirect();
            } else {
                // close popup window and redirect to cancelUrl
                $authIdentity->cancel();
            }
        }

        // Something went wrong, redirect to login page
        $this->redirect(array('site/index'));
    }
    
    public function actionLogout(){
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionIndex() {
        if (defined('YII_DEBUG') && YII_DEBUG) {
            $debug = "true";
        } else {
            $debug = "false";
        }


        $myPackage = array(
            'basePath' => 'application.views.assets.game',
            'js' => array('js/sockjs-0.3.js', 'js/connection-manager.js', 'js/game.js',),
            'css' => array('css/game.css'),
            'img' => array(),
            'depends' => array('jquery', 'jquery.ui')
        );

        $packUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias($myPackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
        Yii::app()->clientScript->addPackage('gamePack', $myPackage)->registerPackage('gamePack');

        $cs = Yii::app()->getClientScript();
        $cs->registerScript(
                'socket-init', "
                        var webSocketAddress = 'http://" . Yii::app()->params['shared']['server'] . ":" . Yii::app()->params['shared']['port'] . "/socket';
                        var siteDebugMode = " . $debug . ";
                     ", CClientScript::POS_BEGIN);
        
        if (!Yii::app()->user->isGuest) {
            $cs->registerScript(
                    'user-init', "
                        var siteUid = '" . Yii::app()->user->getModel()->uid . "';
                        var siteUserLobbyToken = '" . Yii::app()->user->getModel()->generateLobbyToken() . "';
                     ", CClientScript::POS_BEGIN);
        }
        
        Yii::app()->clientScript->registerCssFile(
                Yii::app()->clientScript->getCoreScriptUrl() .
                '/jui/css/base/jquery-ui.css'
        );
        
        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        Yii::getPathOfAlias('application.views.assets.js') . '/active-tab.js'
                )
        );
        
        $this->render('play', array(
            'gameAssetsUrl' => $packUrl,
        ));
    }
}