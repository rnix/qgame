<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Очередь',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
    ),
    //'theme' => 'bootstrap',
    'modules' => array(
        /*
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'Y2k',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'bootstrap.gii',
            ),
        ),
         * 
         */
    ),
    // application components
    'components' => array(
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('site/selectLogin'),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<action:\w+>' => 'site/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host='. $sharedConfig['db']['host'] .';dbname='. $sharedConfig['db']['dbname'],
            'emulatePrepare' => true,
            'username' => $sharedConfig['db']['username'],
            'password' => $sharedConfig['db']['password'],
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true,
            'cache' => false,
            'cacheExpire' => 0, 
            'services' => array(
                'livelevel' => array(
                    'class' => 'LivelevelOAuthService',
                    'client_id' => $sharedConfig['auth']['client_id'],
                    'client_secret' => $sharedConfig['auth']['client_secret'],
                ),
            ),
        ),
        'clientScript' => array(
            'class' => 'ext.minScript.components.ExtMinScript',
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'shared' => $sharedConfig['general'],
    ),
    
    'controllerMap' => array(
        'min' => array(
            'class' => 'ext.minScript.controllers.ExtMinScriptController',
        ),
    ),
);